int ricbin(int lista[], int numric, int primo, int ultimo)				//Prendo in ingresso il vettore, il numero da ricercare e le variabili primo e ultimo
{
	int mezzo;
	if (primo<=ultimo) 													//Se la variabile primo è minore o uguale alla variabile ultimo
	{
		mezzo = (primo + ultimo)/2;										//calcolo il mezzo
		switch(CONFRONTA(lista[mezzo], numric))							//Invoco la funione confronta passando come parametri il valore di mezzo e numric e in base al valore che restituisce..
		{
			case –1 : return(ricbin(lista, numric, mezzo+1, ultimo));	//Richiamo la funzione ricbin considerando solo la parte destra del vettore
			case 0 : return mezzo;										//Restituisco mezzo
			case 1 : return(ricbin(lista, numric, primo, mezzo -1));	//Richiamo la funzione ricbin considerando solo la parte sinistra del vettore
		}
	}
	return -1;
}

int CONFRONTA(int x,int y)							//Funzione confronta
{
    if(x<y) return -1;								//numric sta a destra: restituisci -1
    else  if(x==y) return 0;						//numric sta in mezzo: restituisci 0 (numric trovato)	
    else return 1;									//numric sta a sinistra: restituisci 1
}
