int ricbin(int lista[], int numric, int n)			//Prendo in ingresso il vettore, il numero da ricercare e la dimensione del vettore
{
    int primo=0, ultimo=n-1, mezzo;					//Variabili che segnano l'inizio, la fine e la metà dell'intervallo di ricerca

    while(primo<=ultimo)							//Fin quando la variabile primo è minore o uguale alla variabile ultimo (fin quando ci sono ancora elementi in mezzo)
    {
        mezzo = (primo + ultimo)/2;					//calcolo il mezzo
        switch(CONFRONTA(lista[mezzo], numric))		//Invoco la funione confronta passando come parametri il valore di mezzo e numric e in base al valore che restituisce..
        {
        case -1:
            primo = mezzo + 1;						//considero solo la parte destra del vettore
            break;
        case 0:
            return mezzo;							//oppure restituisco mezzo
        case 1:
            ultimo = mezzo -1;						//oppure considero solo la parte sinistra del vettore
        }
    }
    return -1;
}

int CONFRONTA(int x,int y)							//Funzione confronta
{
    if(x<y) return -1;								//numric sta a destra: restituisci -1
    else  if(x==y) return 0;						//numric sta in mezzo: restituisci 0 (numric trovato)	
    else return 1;									//numric sta a sinistra: restituisci 1
}
