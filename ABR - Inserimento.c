//O(h)

void insert_nodo(tree_pointer *nodo, int num)
{
	tree_pointer ptr, temp;
	temp = ric_modificata(*nodo, num);
	if(temp || !(*nodo)) 
	{
		ptr = (tree_pointer)malloc(sizeof(nodo));
		if (ptr == NULL) 
		{
			printf("\la memoria è piena");
			exit(1);
		}
		ptr->dati = num;
		ptr->figlio_sinistro = NULL;
		ptr->figlio_destro = NULL;
		if(*nodo)
			if(num < temp->dati) temp->figlio_sinistro = ptr;
			else temp->figlio_destro = ptr;
		else *nodo = ptr;
	}
}
