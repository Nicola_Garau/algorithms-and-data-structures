void sort(int lista[], int n)		//Prendo in ingresso il vettore e la sua dimensione
{
 int i,j,min;						//Due contatori di ciclo + una variabile per lo swap

 for(i=0; i<n-1; i++)				//Faccio scorrere (tramite i) tutta la lista sino al penultimo elemento
  {
    min = i;						//Assegno a min il valore dell'elemento corrente
    for(j=i+1; j<n; j++)			//Faccio scorrere (tramite j) tutta la lista a partire dall'elemento corrente sino ad arrivare all'ultimo elemento (non penultimo)
       if(lista[j] < lista[min])  	//Se il valore puntato da j è minore di quello puntato da i
       	min = j;					//assegno a min il valore di j, così so in che posizione si trova l'elemento più piccolo
    SWAP(&lista[i], &lista[min]);	//Invoco la funzione swap passando come parametri l'elemento alla cella i-esima e quello alla cella min-esima
  }
}

void SWAP(int *x, int *y)			//Funzione di swap
{
 int temp;							//Variabile temporanea

 temp = *x;							//Assegno a temp il valore di x,
 *x = *y;							//a x il valore di y
 *y = temp;							//e infine a y il valore di temp
}