void trasp_rapida(termine a[], termine b[])
{
	/** Dichiaro i due vettori che mi permettono di eseguire la trasposizione rapida
		Il primo, termini_riga, mi permette di sapere quanti elementi non nulli ci sono nella colonna i-esima della
		matrice trasposta originale (ossia a), ovvero il numero di elementi non nulli presenti nella i-esima riga 
		della matrice di arrivo (ossia b).
		Il secondo mi permette di memorizzare la posizione di ogni primo elemento di valore k-esimo all'interno del vettore
		di partenza (a);
	**/
	int termini_riga[MAX_COL], pos_iniziale[MAX_COL];
	int i,j, num_col = a[0].col, num_termini=a[0].valore;	//due contatori + due variabili nelle quali segno il numero di  colonne ed elementi non nulli della matrice originale 
	b[0].riga = num_col; b[0].col = a[0].riga;				//assegno i valori invertiti del numero di righe e colonne alla matrice b
	b[0].valore = num_termini;								//e copio anche il numero di termini						
	if(num_termini>0) 					//Se ci sono elementi non nulli									
	{
		for(i=0; i<num_col; i++) 		//faccio scorrere il numero di colonne della matrice di partenza
			termini_riga[i]=0;			//e azzero il valore di ogni cella perchè poi dovrò incrementarlo
		
		for(i=1; i<=num_termini; i++) 	//Faccio scorrere tutti gli elementi della matrice originale, saltando la cella 0
			termini_riga[a[i].col]++;	//prendo il valore corrente della colonna e lo uso come indice per il vettore di termini, incrementando di un valore la cella indicata dall'indice
										//in questo modi so esattamente quanti elementi saranno presenti alla riga a[i].col-esima (n.b. le colonne della matrice a equivalgono alle righe della matrice b)
		
		pos_iniziale[0]=1;				//Il primissimo elemento deve stare in posizione 1
		for(i=1; i<num_col; i++)		//Faccio scorrere tutte le colonne della matrice di partenza, ossia tutte le righe della metrice di arrivo
			pos_iniziale[i] = pos_iniziale[i-1]+termini_riga[i-1]; //e calcolo la posizione iniziale sommando la posizione iniziale del primo elemento della scorsa riga + il numero di termini appartenenti a quella riga

		for(i=1; i<=num_termini; i++) 	//Faccio scorrere tutti gli elementi della matrice di partenza a partire dalla cella 1
		{
			j=pos_iniziale[a[i].col]++;	//Leggo il valore presente all'interno del vettore pos_iniziale per quanto riguarda la colonna corrente,
										//assegno questo valore a j e solo dopo incremento il valore della cella di pos_iniziale
			b[j].riga = a[i].col;		//così ora posso effettuare lo scambio, al prossimo ciclo, se sono presenti altri elementi nella stessa colonna del ciclo corrente, 
										//questi non verranno memorizzati più nella stessa cella, ma in quella successiva, perchè pos_iniziale[colonna corrente] è stato incrementato
			b[j].col = a[i].riga;		//N.B. il vettore pos_iniziale viene utilizzato per disporre correttamente gli elementi all'interno della matrice NUOVA!!
			b[j].valore = a[i].valore;
		}
	}
}