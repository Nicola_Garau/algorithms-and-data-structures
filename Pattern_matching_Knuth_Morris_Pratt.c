void insucc(char *pat)
{
/* Calcola la funzione insuccesso per
un pattern (pat) */
	int i,j;				//Due contatori, i serve a memorizzare l'insuccesso della cella precedente a j e j è l'indice che serve per scorrere le lettere del pattern
	int n = strlen(pat);	//Lunghezza del pattern
	insuccesso[0] = -1;		//Assegno ala prima cella del vettore insuccesso il valore -1
	for(j=1; j<n; j++)							//Faccio scorrere tutte le lettere del pattern tranne la prima
	{
		i=insuccesso[j-1];						//Assegno a i il valore della cella precedente del vettore insuccesso
		while((pat[j]!=pat[i+1]) && (i>=0))		//Fin quando la lettera corrente del pattern è diversa dalla lettera del pattern puntata dalla variabile i+1 (insuccesso della cella precedente+1) ed i>=0
			i = insuccesso[i];					//i prende il valore di insuccesso di i	(necessario il while per le sottostringhe)	
		if (pat[j] == pat[i+1])					//se la lettera puntata è uguale alla lettera puntata dalla variabile i+1 (valore della funzione insuccesso della cella precedente aumentato di uno) 
			insuccesso[j]=i+1;					//il valore della funzione insuccesso per la cella j viene incrementato di 1 rispetto al precedente
		else insuccesso[j] = -1;				//altrimenti prende come valore -1
	}
}

int pmatch(char *stringa, char *pat)
{
	int i=0, j=0;					//Due contatori
	int lens = strlen(stringa);		//lunghezza della stringa
	int lenp = strlen(pat);			//lunghezza del pattern
	while(i<lens && j<lenp)			//Analizzo tutte le lettere del pattern o della stringa, quando arrivo alla fine di una delle due termino il ciclo
	{
		if(stringa[i] == pat[j]) 	//Se le due celle sono uguali
		{ 
			i++; j++; 				//mi sposto alla cella successiva, sia del pattern che della stringa
		}
		else if (j==0) 				//Se non sono uguali e sto analizzando la prima lettera del pattern
			i++;					//mi sposto alla cella successiva solo della stringa
		else j=insuccesso[j-1]+1;	//Altrimenti, se non sono uguali e non sono alla prima lettera del pattern, 
									//sposto j alla cella corrispondente al valore della funzione insuccesso della cella precedente aumentato di uno
									//Detto in parole semplici: se sinora tutte le lettere analizzate sono risultate uguali a quelle della stringa ma improvvisamente
									//ne incontro una diversa, mi sposto alla cella successiva alla cella "gemella" della cella precedentemente analizzata (che magari era risultata corrispondente alla lettera della stringa)
	}
	return ( (j==lenp) ? (i-lenp) : -1);	//se ho finito di analizzare tutte le lettere del pattern, restituisco il punto di partenza del pattern nella stringa, altrimenti -1
}