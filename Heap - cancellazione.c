//O(log(n))

elemento delete_max_heap(int *n)						//Prendo in ingresso il puntatore al numero di elementi dell'albero (verrà modificato)
{
    elemento item, temp;								//Dichiaro due variabili temporanee per muovermi all'interno dell'albero
    int padre, figlio;									//Dichiaro due interi che identificano il nodo padre ed il nodo figlio
    item = heap[1];										//Inizializzo la variabile item con il valore contenuto nel nodo radice
    temp = heap[(*n) --];								//..e la variabile temporanea con il valore contenuto nell'ultimo nodo dell'albero, successivamente decremento di uno il numero di nodi presenti
    padre = 1;											//Inizializzo la variabile padre ad 1, perchè il padre si trova in posizione 1 nell'array (lo 0 è inutilizzato)
    figlio = 2;											//..e la variabile figlio a 2, infatti il figlio sinistro si trova sempre in posizione 2*i rispetto al padre i
    while (figlio<=*n)									//Fin quando ci sono ancora figli da analizzare
    {													//(SCELGO IL FIGLIO CHE HA LA CHIAVE MAGGIORE)
        if ((figlio < *n) &&							//Se il nodo figlio non è l'ultimo nodo dell'heap
        heap[figlio].chiave<heap[figlio+1].chiave))		//e se il valore contenuto nel figlio sinistro è minore di quello contenuto nel figlio destro
            figlio ++;									//mi sposto verso il figlio destro 
        if (temp.chiave >= heap[figlio].chiave)			//Se il valore contenuto nell'ultimo nodo dell'albero è maggiore o uguale a quello contenuto nel figlio analizzato
            break;										//...ho finito la ricerca, quindi esco
        heap[padre] = heap[figlio];						//Altrimenti scambio il valore contenuto nel padre con quello contenuto nel figlio
        padre = figlio;									//Il figlio diventa padre, 
        figlio *= 2;									//il figlio sinistro del figlio diventa figlio (mi sposto in basso)
    }
    heap[padre] = temp;									//Ho finito la ricerca, sostiituisco al valore contenuto nel nodo radice (che ora si trova in posizione "padre" all'interno dell'array) il valore salvato in temp
    return (item);										//Restituisco in uscita il valore dell'elemento cancellato (il nodo radice)
}
