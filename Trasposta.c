void trasponi(termine a[], termine b[])
{
	int n, i, j, bcorrente;			//numero di elementi non nulli, contatori, indice per far scorrere gli elementi della matrice b
	n = a[0].valore; 				//copio in n il numero di elementi non nulli della matrice
	b[0].riga = a[0].col; 			//memorizzo il numero di colonne della matrice originale (a), che ora diventa il numero di righe della matrice b
	b[0].col = a[0].riga; 			//ed il numero di righe della matrice a diventa il numero di colonne della matrice b
	b[0].valore = a[0].valore;		//il numero di elementi non nulli invece rimane invariato
	
	if(n>0) 							//Se sono presenti degli elementi non nulli
	{
		bcorrente = 1;					//mi sposto alla cella 1 del vettore b (la 0-esima l'ho già inizializzata)
			for(i=0; i<a[0].col; i++) 	//faccio scorrere tutte le colonne della matrice di partenza (ossia tutte le righe della metrice di arrivo)
				for(j=1; j<=n; j++) 	//ed internamente tutti gli elementi della matrice trasposta a
					if(a[j].col == i) 						//Quando trovo un elemento nella matrice a il cui valore di colonna è uguale a quello analizzato (ossia i)
					{
						b[bcorrente].riga = a[j].col;		//assegno a b in posizione bcorrente i valori inveriti di riga e colonna dell'elemento corrente della matrice a	
						b[bcorrente].col = a[j].riga;		
						b[bcorrente].valore = a[j].valore;	//ed il valore, che rimane invariato
						bcorrente++;						//dopo aver riempito la cella mi sposto alla cella successiva nel vettore di arrivo
					}
	}
}