//O(h)

tree_pointer ric_ric(tree_pointer radice, int chiave)
{
	/* fornisce un puntatore al nodo che contiene item
	se tale nodo non esiste fornisce NULL */
	if (!radice) return(NULL);
	if (chiave == radice->dati) return(radice);
	if (chiave < radice->dati) return(ric_ric(radice->figlio_sinistro, chiave));
	return(ric_ric(radice->figlio_destro, chiave));
}