int ntrova(char *stringa, char *pat)
{
	int i,j;									//Due contatori
	int lens = strlen(stringa);					//Salvo in lens la lunghezza della stringa
	int lenp = strlen(pat);						//e in lenp quella del pattern
	for(i=0, j=0; i<lens && j<lenp; i++, j++)	//Un ciclo che va da 0 a lens (o lenp, chi arriva prima); vengono incrementate sia i che j
		while (stringa[i]!=pat[j]) 				//Fin quando la lettera corrente della stringa è diversa dalla corrente del pattern..		
			{
				i = i -(j-1); 					//Torno indietro nella stringa j-1 volte, ossia tanto quanto le lettere del pattern corrispondenti analizzate sinora -1
				j=0; 							//e torno all'inizio del pattern...MA QUESTO NON SI FERMA MAI SE LE DUE STRINGHE SONO TOTALMENTE DIVERSE!!	
			}
	if(j == lenp) 								//Se a fine ciclo j vale lenp, vuol dire che il pattern è contenuto nella stringa
		return i-lenp;							//Quindi restituisco il valore corrente di i-lenp, ossia l'indice della stringa dal quale parte la corrispondenza del pattern
	else return -1;								//Altrimenti restituisco -1
}