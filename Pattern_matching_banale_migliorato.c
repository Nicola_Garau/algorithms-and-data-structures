int ntrova(char *stringa, char *pat)
{
	int i,j, start=0;									//Due contatori più un segnaposto per il punto di partenza del pattern all'interno della stringa
	int lasts = strlen(stringa) - 1;					//Segnaposto per l'ultima lettera della stringa
	int lastp = strlen(pat) - 1;						//e per l'ultima del pattern
	int endmatch = lastp;								//Variabile che serve per sapere quando terminare il ciclo, la inizializzo col numero indicante l'ultima lettera del pattern
	for(i=0; endmatch<=lasts; endmatch++, start++)		//Fin quando endmatch è minore del numero di lasts
	{
		if(stringa[endmatch] == pat[lastp])				//Se l'ultima lettera (lastp-esima) del pattern è uguale alla endmatch-esima della stringa (che all'inizio del ciclo è uguale alla lastp-esima)
			for(j=0, i=start; j<lastp && stringa[i]==pat[j]; i++, j++) ;	//se le due lettere confrontate coincidono e j<lastp (partendo da j=0 e i=start), eseguo il ciclo ed incremento i e j
				if(j == lastp) 												//se ho finito le lettere del pattern
					return start;											//restituisco start, che è il punto di partenza del pattern all'interno della stringa
	}													//Altrimenti incremento endmatch e start
	return -1;											//Se non coincidono, restituisco -1
}