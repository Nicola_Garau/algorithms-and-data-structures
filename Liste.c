/**Defines**/
#define IS_FULL(ptr) (!(ptr))
#define IS_EMPTY(ptr) (!(ptr))

/**Creazione di una lista**/
typedef struct list_node *list_pointer;	//Nuovo tipo, puntatore alla struttura list_node 
typedef struct list_node				//Struttura list_node
{
    int key;							//Campo chiave
    list_pointer link;					//Riferimento alla struttura interno alla struttura (uso il tipo list_pointer definito in precedenza)
} lista;								//Creo il nuovo tipo lista di tipo struct list_node
list_pointer testa = NULL;				//Dichiaro la testa della lista e la inizializzo a NULL

/**Visualizzazione**/
void visualizza(list_pointer ptr)		//Prendo in ingresso il puntatore alla lista
{
    if (IS_EMPTY(ptr))					//Se la lista è vuota
    {
        printf("\nLa lista e' vuota");	//Avviso ed esco
        return;
    }
    printf("\nLa lista contiene: ");	//Altrimenti la stampo
    for(; ptr; ptr = ptr->link)			//Partendo da ptr e passando ad ogni ciclo al link contenuto in ptr fin quando ci sono elementi non nulli
        printf("%4d", ptr->key);		
    printf("\n");
}

/**Inserimento in testa**/
void insert_testa(list_pointer *ptr, int item)		//
{
    list_pointer temp;
    temp = (list_pointer)malloc(sizeof(list_node));
    if(IS_FULL(temp))
    {
        fprintf(stderr, "La memoria e' piena");
        exit(1);
    }
    temp->key = item;
    temp->link = *ptr;
    *ptr = temp;
}

/**Inserimento nel mezzo**/
void inserisci(list_pointer *testa, int item)
{
    list_pointer nodo, ptr;
    ptr = *testa;
    nodo = ptr;
    if(ptr && ptr->key > item) insert_testa(testa, item);
    else
    {
        for(; ptr && ((ptr->key)<item); ptr=ptr->link ) nodo = ptr;
        insert_dopo(testa, nodo, item);
    }
}

/**Inserimento in coda**/
void insert_dopo(list_pointer *ptr, list_pointer nodo, int item)
{
    list_pointer temp;
    temp = (list_pointer)malloc(sizeof(list_node));
    if(IS_FULL(temp))
    {
        fprintf(stderr, "La memoria e' piena");
        exit(1);
    }
    temp->key = item;
    if (*ptr)
    {
        temp->link = nodo->link;
        nodo->link = temp;
    }
    else
    {
        temp->link = NULL;
        *ptr = temp;
    }
}

/**Cancellazione in testa o in mezzo**/
void cancella(list_pointer *testa, int item)
{
    list_pointer nodo, ptr;
    if (IS_EMPTY(*testa))
    {
        printf("\nLa lista e' vuota");
        return;
    }
    ptr = *testa;
    for(; ptr && ((ptr->key)!=item); ptr=ptr->link )
        nodo = ptr;
    if (ptr == NULL) printf("\nL'elemento non e' presente nella lista");
    else
    {
        if (ptr == *testa) nodo = NULL;
        cancella_dopo(testa, nodo, ptr);
    }
}

/**Cancellazione in coda**/
void cancella_dopo(list_pointer *testa, list_pointer nodo, list_pointer ptr)
{
    if(nodo) nodo->link = ptr->link;
    else *testa = (*testa)->link;
    free(ptr);
}
