//O(h)

tree_pointer ric_iter(tree_pointer tree, int chiave)
{
    /* fornisce un puntatore al nodo che contiene item
    se tale nodo non esiste fornisce NULL */
    while(tree) 
    {
        if (chiave == tree->dati) return(tree);
        if (chiave < tree->dati) tree = tree->figlio_sinistro;
        else tree = tree->figlio_destro;
    }
    return(NULL);
}
